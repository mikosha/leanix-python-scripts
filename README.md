# LeanIX Python Scripts

The project includes python scripts that capture recurring little tasks with the help of LeanIX APIs.

# Setup

- Downoad the latest version of Python's Integrated Development Environment [here](https://www.python.org/downloads/). 
- Install Pandas by running the following command in a terminal of your choice, `python3 -m pip install --upgrade pandas`
- Install Requests by running the following command in a terminal of your choice, `python3 -m pip install --upgrade requests`

If the above commands do not work, replace "python3" with "python".

NOTE: You need to replace the following variables in order to make the scripts work for your LeanIX environment.

- api_token = 'An API Token from the Teranet Sandbox or Production environments'
- auth_url = 'https://teranet.leanix.net/services/mtm/v1/oauth2/token'
- request_url = 'https://teranet.leanix.net/services/pathfinder/v1/graphql' 

For information on how to create an API token click [here](https://docs.leanix.net/docs/create-and-manage-your-own-api-tokens).

# Getting Started

- Download the required Python scripts [here](https://github.com/leanix-public/scripts). Open the script in an IDE of your choice and modify the source code to accomodate required changes.

# Running the script

- Click on the Run Module command under Run in the menu bar to execute the finished script.
